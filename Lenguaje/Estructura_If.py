Edad = int(input("Ingrese la edad a evaluar\n> "))
Max = int(input("Ingrese la edad maxima\n> "))

if Edad < Max:
    print("Tienes menos de %d " % Max)


if Edad < Max:
    print("Tienes menos de %d " % Max)
else:
    print("Tienes mas de %d " % Max)

#Condicionales
#----------------- if y operadores relacionales
if 2 < 5:
    print ("2 es menor que 5")
if 2 > 5:
    print ("2 es mayor que 5")
if 2 <= 5:
    print ("2 es menor igual que 5")
if 2 >= 5:
    print ("2 es mayor igual que 5")
if 2 != 5:
    print ("2 es diferente que 5")
if 2 == 5:
    print ("2 es igual que 5")

#------------------------- if - elif
if 2 > 5:
    print(True)
elif 2 < 5:
    print (False)


#------------------------- if - elif - else
if 2 > 5:
    print(True)
elif 2 == 5:
    print (False)
else:
    print ("Else ese imprime si lo anterior es",False)

#------------------------- if - cortos
if 2 < 5 : print ("If de una linea")

print ("Cuando devuelve verdader") if 5 > 2 else print("Cuando devuelve false")

#------------------------- Condiciones anidadas
if 2 < 5 and 2 != 5: #las dos condiciones deven de ser verdaderas
    print(True) 

if 2 < 5 or 2 == 5: #Una de las dos condicions deve de ser verdadera
    print(True)

