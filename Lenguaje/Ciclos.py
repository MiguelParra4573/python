#Loops While

i= 0
while i < 5:
    print("Vuelta",i)
    if i == 3:
        break    
    i+=1

# i= 0
# while i < 5:
#     print("Vuelta",i)
#     if i == 3:
#         continue    
#     i+=1


#Loops For
usuarios =["Miguel", "Jose", "Mathyas", "Alejandro", "Nestor", "Alonso"]
for usuario in usuarios:
    if usuario == "Mathyas":
        break
    print(usuario)
    

Nombre = "Miguel Parra"
for c in Nombre:
    if usuario == "Mathyas":
        continue
    print(c)
    
for x in range(5, 20): #Tambien se puede usar con rangos
    print(x)

#A los cuales tambien se les puede definir un numero de incremento
for x in range(5, 20, 3): 
    print(x)

#Aqui tambien se puede ejectar la sentencia -> else:
for x in range(5, 20, 3): 
    print(x)
else:
        print("El ciclo a terminado")

#anidacion de fors

Edades =  [10,11,12,13,14]

for usuario in usuarios:
    for edad in Edades:
        print(usuario, edad)


#Recursividad
def recursion(i):
    if i < 1:
        return i
    print(i)
    recursion(i - 1)

recursion(7)





